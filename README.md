This repo has ROFOS cmap 2020 material of Web track mainly JavaScript.

Covered concepts/topics:

0.Aim of learining JS. What you will learn and what not. What to expect and what not to.

**Introduction to JS [10 min]**
    What is JS, what is engine, how does that work bla bla.
    JS standards. Latest stable standerds.

**Console setup [5 min]**
    Show demo by opeing a console on a website.

**Fundamentals [5 min]**
    How to write JS in html? How to link JS file into html. Different ways.
    Rules to place js file. Best practices.
    **Task:**
    Try doing alert with both internal and external JS.
    
**1.Variables, constants, variable naming, data types, operators, Comparisons, [30-40 min]**
    **Task:**
    Make simple math operations in console.
    OR
    1. Declare two variables: admin and name.
    2. Assign the value "John" to name.
    3. Copy the value from name to admin.
    4. Show the value of admin using alert (must output “John”).

**2.Conditional operators, Loops **[45 min]****
    **Functions, Function callbacks **[1 hour]****
    **Task 1**
    Using if..else, write the code which gets a number via prompt and then shows in alert:

    1, if the value is greater than zero,
    -1, if less than zero,
    0, if equals zero.
    In this task we assume that the input is always a number.
    
    **Task 2**
    Taks on a for loop

**3.Objects, Object methods, "this", array accessing **[1 hour]**** 

**4 Document, DOM**
    Task:
    Do a Miles to KM conversion. 

**5.Introduction to browser events **[1 hour]****

**6.Forms **[1 hour]****

7.Final Project **[1 hour]**



Few questions as homework.

1. Who invented internet?
2. Who invented JavaScript?
3. What is WWW?
4. Which was the first browser?
5. How internet works?
6. What is the difference between webpage, website, Web server, and search engine?
7. Does anyone own the internet?
8. What is a protocol?