//Page fade in
document.body.classList.add('fade');

document.addEventListener("DOMContentLoaded", function(e) {
    document.body.className = '';
  });
////////////////////////////////////////

//Fade in animation 
// Sticky navigation
let nav = document.getElementById('sticky-nav');
let sticky = nav.offsetTop;

function setSticky(){
    if(window.pageYOffset > sticky){
        nav.classList.add('sticky');
    }
    else{
        nav.classList.remove('sticky');
    }
}
window.onscroll = function() {setSticky()};
////////////////////////////////////////////

const price = 800;
var inputValue = document.getElementById('input-count');
var rangeSlider = document.getElementById('input-ranger');

function calculatePrice(val){
  inputValue.value = val;
  var rangerValue = rangeSlider.value;
  var showResult = document.getElementById('output');
  showResult.innerHTML = rangerValue * price;
}

function changeInput(val){
  rangeSlider.value = val;
}